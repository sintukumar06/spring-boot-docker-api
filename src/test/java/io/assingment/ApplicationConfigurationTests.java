
package io.assingment;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class ApplicationConfigurationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGreetingWithInvalidAccount() throws Exception {
        ResponseEntity<String> entity = restTemplate
                .getForEntity("http://localhost:" + this.port + "/greeting?account=test&id=100", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
        assertThat(entity.getBody(), Matchers.containsString("greeting.account: must match"));
    }

    @Test
    public void testGreetingWithPersonalAccountWithPositiveId() throws Exception {
        ResponseEntity<String> entity = restTemplate
                .getForEntity("http://localhost:" + this.port + "/greeting?account=personal&id=100", String.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals("Hi, userId 100", entity.getBody());
    }

    @Test
    public void testGreetingWithPersonalAccountWithNegativeId() throws Exception {
        ResponseEntity<String> entity = restTemplate
                .getForEntity("http://localhost:" + this.port + "/greeting?account=personal&id=-120", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
        assertThat(entity.getBody(), Matchers.containsString("greeting.id: must be greater than or equal to 0"));
    }

    @Test
    public void testGreetingWithBusinessAccountHavinginvalidType() throws Exception {
        ResponseEntity<String> entity = restTemplate
                .getForEntity("http://localhost:" + this.port + "/greeting?account=business&type=invalid", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
        assertThat(entity.getBody(), Matchers.containsString("greeting.type: must match"));
    }

    @Test
    public void testGreetingWithBusinessAccountMissingType() throws Exception {
        ResponseEntity<String> entity = restTemplate
                .getForEntity("http://localhost:" + this.port + "/greeting?account=business", String.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
        assertEquals("Account type required for business", entity.getBody());
    }

    @Test
    public void testGreetingWithBusinessAccountOfSmallBusiness() throws Exception {
        ResponseEntity<String> entity = restTemplate
                .getForEntity("http://localhost:" + this.port + "/greeting?account=business&type=small", String.class);
        assertEquals(HttpStatus.NOT_ACCEPTABLE, entity.getStatusCode());
        assertEquals("Small business is not yet implemented", entity.getBody());
    }

    @Test
    public void testGreetingWithBusinessAccountOfBigBusiness() throws Exception {
        ResponseEntity<String> entity = restTemplate
                .getForEntity("http://localhost:" + this.port + "/greeting?account=business&type=big", String.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals("Welcome, business user!", entity.getBody());
    }

}
