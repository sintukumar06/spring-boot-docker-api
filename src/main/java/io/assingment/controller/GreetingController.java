package io.assingment.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import static java.util.Objects.nonNull;
import static javax.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@Validated
@RestController
public class GreetingController {

    public static final String PERSONAL = "personal";
    public static final String BUSINESS = "business";

    @GetMapping(value = "/greeting", produces = TEXT_PLAIN_VALUE)
    public ResponseEntity<String> greeting(@RequestParam(name = "account") @Pattern(regexp = "business|personal", flags = CASE_INSENSITIVE) String account,
                                           @RequestParam(name = "id", required = false) @Min(0) Integer id,
                                           @Pattern(regexp = "small|big", flags = CASE_INSENSITIVE)
                                           @RequestParam(name = "type", required = false) String type) {
        if (account.equals(PERSONAL)) {
            return new ResponseEntity("Hi, userId " + id, OK);
        } else if (account.equals(BUSINESS) && nonNull(type)) {
            if (type.equals("big"))
                return new ResponseEntity("Welcome, business user!", OK);
            else if (type.equals("small"))
                return new ResponseEntity("Small business is not yet implemented", NOT_ACCEPTABLE);
        } else
            return new ResponseEntity("Account type required for business", BAD_REQUEST);
        return new ResponseEntity("Requested account not allowed", BAD_REQUEST);
    }
}
